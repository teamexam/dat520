
interface IJobHandler {
    submit(job: any);
    addEventListener(type: "Confirm", callback: (job: any) => void);
}

class SynchronousJobHandler implements IJobHandler {
    events: EventTarget = new EventTarget();
    submit(job: any) {
        // Processes job
        this.events.dispatchEvent(new CustomEvent("Confirm", job));
    }

    addEventListener(type: "Confirm", callback: (job: any) => void) {
        this.events.addEventListener(type, callback);
    }
}

class AsyncJobHandler implements IJobHandler {
    events: EventTarget = new EventTarget();
    buffer: any[] = [];

    constructor() {
        setInterval(() => {
            if (this.buffer.length > 0) {
                const job = this.buffer.shift();
                // process job
            }
        }, 100)
    }

    submit(job: any) {
        this.buffer.push(job);
        this.events.dispatchEvent(new CustomEvent("Confirm", job));
    }

    addEventListener(type: "Confirm", callback: (job: any) => void) {
        this.events.addEventListener(type, callback);
    }


}