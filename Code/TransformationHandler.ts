
interface TransformationHandler{
    Submit(job: any);
    addEventListener(type: "Confirm", callback: (job: any) => void);
    addEventListener(type: "Error", callback: (job: any) => void);
}

class JobTransformationByBuffering implements TransformationHandler {
    events: EventTarget = new EventTarget();
    top = 1;
    bottom = 1;
    handing = false;
    M = 100;
    buffer: any[] = [];

    jobHandler: IJobHandler;
    constructor(jobHandler: IJobHandler) {
        this.jobHandler = jobHandler;
        this.jobHandler.addEventListener("Confirm", (job) => this.onConfirm(job));
        setInterval(() => {
            if (this.bottom < this.top && this.handing === false) {
                const job = this.buffer[this.bottom % this.M + 1];
                this.bottom++;
                this.handing = true;
                jobHandler.submit(job);
            }
        }, 100)
    }

    Submit(job: any) {
        if (this.bottom + this.M == this.top) {
            this.events.dispatchEvent(new CustomEvent("Error", job));
        } else {
            this.buffer[this.top % this.M + 1] = job;
            this.top = this.top + 1;
            this.events.dispatchEvent(new CustomEvent("Confirm", job));
        }
    }

    addEventListener(type: "Confirm", callback: (job: any) => void);
    addEventListener(type: "Error", callback: (job: any) => void);
    addEventListener(type: string, callback: any) {
        this.events.addEventListener(type, callback);
    }

    onConfirm(job: any) {
        this.handing = false;
    }
}