<style>
    .hoverover{
        color: white;
        background-color: white;
    }

    .hoverover img{
        display: none;
    }

    .hoverover:hover{
        color: black;
    }

    .hoverover:hover img {
        display: block;
    }
</style>

# Chapter 3

## Eager probabilistic broadcast
Eager probabilistic broadcast (Alg 3.9) uses two parameters, *k* and *R*.
Let *P* denote the probability that all processes receive a certain message using eager probabilistic broadcast.
1. What happens with *P* if we increase *R*?

    __Answer:__
    <div class="hoverover">
    If we increase R, P will also increase towards 1, but not necessarily reach 1.
    </div>

2. Can you choose *k* and *R* such that *P=1*?
  
    __Answer:__
    <div class="hoverover">
    We can choose k=N and R=1, then P=1 holds, unless the sender fails.
    </div>

## Uniform reliable broadcast
Assume 5 processes are running a uniform reliable broadcast module. If 3 of the processes fail, which of the properties from uniform reliable broadcst (module 3.3) still hold if they are running
* Algorithm 3.4 All-Ack Uniform Reliable Broadcast

    __Answer:__
    <div class="hoverover">
    All properties still hold, since the algorithm tolerates N-1 processes failing.
    </div>

* Algorithm 3.5 Majority-Ack Unifrom Reliable Broadcast

    __Answer:__
    <div class="hoverover">
    URB2 and URB3 (No Creation and No duplication) still hold. They are safety properties. Uniform agreement and Validity do no longer hold. These are liveness properties.
    </div>

* Algorithm 3.4, and the perfect failure detector fails to meet the *strong accuracy* property?

    __Answer:__
    <div class="hoverover">
    Without accuracy, processes can be falsy detected to have failed. Uniform agreement can be violated.
    </div>

* Algorithm 3.4 and the perfect failure detector fails to meet the *strong completeness* property?

    __Answer:__
    <div class="hoverover">
    Without completeness, crashed processes may never be detected. All liveness properties (Uniform agreement and validity) can be violated.
    </div>

## FIFO and Causal broadcast

1. If we replace the ReliableBroadcast module (*rb*) in algorithm 3.12 with BestEffordBroadcast (*beb*), 
which properties of Module 3.8 (FIFOReliableBroadcast) does the resulting algorithm still implement?

    __Answer:__
    <div class="hoverover">
    FIFO delivery still holds. Validity, No creation and No duplication hold as well, but Agreement does no longer hold.
    </div>

2. If we replace the BestEffordBroadcast module *beb* in algorithm 3.3 (Eager Reliable Broadcast) with a FIFOReliableBroadcast (*frb*),
does the resulting algorithm implement *CAUSAL ORDER reliable broadcast*?

    __Answer:__
    <div class="hoverover">
    Yes, the result implements CAUSAL ORDER reliable broadcast.
    </div>

## Liveness and Safety

1. Compare the following property to the **causal delivery** property (**CRB5**) of module 3.9:

    *If a process delivers messages m1 and m2 and m1->m2 then the process must deliver m1 before m2.*

    __Answer:__
    <div class="hoverover">
    See Solution 3.11 in the book.
    <img src="fig/Solution_3_11.png">
    </div>

*If a process delivers messages m1 and m2 and m1->m2 then the process must deliver m1 before m2.*

2. Is the above property a liveness or safety property?

    __Answer:__
    <div class="hoverover">
    A safety property. It states that something bad does not happen. If violated, it is violated in a finite prefix of an execution.
    </div>

# Chapter 4
## Definitions

1. What is a *failed* and a *completed* operation?

    __Answer:__
    <div class="hoverover">
    A completed operation is an invokation with a matching return. A failed operation is an invoked operation, where the process fails before the operation returns.
    </div>

2. What is a *sequential* execution?

    __Answer:__
    <div class="hoverover">
    An execution where every two operations are sequential, that meens, one operation returns before another operation is invoked. This includes operations invoked at different processes.
    </div>

3. Draw a sequence diagram for a sequential execution of a (1,N)-register, 
including 2 write and 2 read operations.

    __Answer:__
    <div class="hoverover">
    Figure 4.3 is a sequential execution. Just add one more read at the end.
    <img src="fig/Figure_4_3.png">
    </div>

## Regular  vs. atomic

4. Is the execution you have dawn in point 3 regular? Is it atomic?

    __Answer:__
    <div class="hoverover">
    Execution in Fig. 4.3 is not regular, therefore not atomic. 
    To be regular the read in Fig. 4.3 must return the last value written, `y`
    <img src="fig/Figure_4_3.png">
    </div>

5. Draw a sequence diagram for an execution of a (1,N)-register 
that is not regular.

    __Answer:__
    <div class="hoverover">
    See Fig. 4.3
    <img src="fig/Figure_4_3.png">
    </div>

6. Draw a sequence diagram for an execution of a (1,N)-register 
that is regular but not atomic.

    __Answer:__
    <div class="hoverover">
    See Fig. 4.4 and 4.5
    <img src="fig/Figure_4_4.png">
    <img src="fig/Figure_4_5.png">
    </div>

*Optional:* Think of if the sequence diagram you have drawn is possible for algorithm 4.1 or 4.2 and 
how/why this behaviour is not possible in algorithm 4.5 or 4.6.

__Answer:__
<div class="hoverover">
Fig. 4.4 is not possible in Alg. 4.1, Fig 4.5 is possible in Alg. 4.2, Fig 4.6 is possible in 4.2
</div>

## Atomic and linearizable

7. The figure below shows an execution of an (N,N)-register.
  * Show that the execution is atomic, by assigning every operation a synchronization point.
  * Show that the execution is linearizable by ordering the operations.

![Figure 1, an execution of an (N,N)-register](fig/CH4-fig1.jpg?raw=true)

__Answer:__
<div class="hoverover">
The order is `W(x), R()-> x, W(y), R() -> y, R() -> y, R() -> y, W(z), R() -> z`
The figure below shows the synchronization points for showing that the execution is atomic. Note that if you order the operations according to their points, the resulting order is the same one as the order above.
<img src="fig/CH4-Ex-Answer.png">
</div>

8. The two figures below shows an execution of an (N,N)-register with one failed write operation. 
  Remember that failed operations may or may not be included in the order and my pr may not get a synchronization point.
  * Show that the execution is atomic, by assigning every operation a synchronization point.
  * Show that the execution is linearizable by ordering the operations.
  
 ![Figure 2, an execution of an (N,N)-register with a failed write.](fig/CH4-fig2.jpg?raw=true)

__Answer:__
<div class="hoverover">
We only write down the order. Note that we don't need to include the failed operation in the order.*
`W(x), R()->x, R()->x, W(z), R()->z, R()->z`
</div>
 
 ![Figure 3, an execution of an (N,N)-register with a failed write.](fig/CH4-fig3.jpg?raw=true)

__Answer:__
<div class="hoverover">
Same as above. But we need to include the completed read operation, that return `y`. We therefore also need to include the `W(y)`, so the result gives a valid sequetial execution.*
`W(x), R()->x, W(y), R()->y, W(z), R()->z, R()->z`
</div>

## Linearizability on a set-object
*difficult exercise*

The point of the following exercise is to see that linearizability or atomicity are meaningfull properties, 
in the context if other distributed objects, than registers.
We therefore look at a set object:

The set has three operations:
 * `add(_)` adds an element to the set
 * `rem(_)` removes an element from the set, if it is part of the set
 * `show()` returns the set, including all elements that have been added but not removed
 
Figure 4 shows a sequential execution of our set. Note that two adds do not result in duplicates.
 
 ![Figure 4, a sequential execution of our set.](fig/CH4-fig4.jpg?raw=true)
 
 
9. The figure below shows an execution of our set. Show that the execution is atomic/linearizable, by assigning synchronization points and ordering operations.

![Figure 5, a concurrent execution of our set.](fig/CH4-fig5b.jpg?raw=true)


*!! There earlier was a mistake in this figure. It is now fixed. The earlier figure (now Figure 6) is not linearizable (atomic). It is still shown below.*

![Figure 6, a concurrent execution of the set object, that is not linearizable.](fig/CH4-fig5.jpg?raw=true)


# Chapter 5

## Uniform and regular consensus

* What is the difference between uniform an regular consensus?

    __Answer:__
    <div class="hoverover">
    In uniform consensus, also faulty processes need to agree and cannot output a different value. In regular consensus, a process may decide on one value, then crash and the other processes then decide on a different value.*
    </div>

* Does uniform consensus implement regular consensus or vice versa?

    __Answer:__
    <div class="hoverover">
    Uniform consensus does implement regular consensus. This means every algorithm for uniform consensus also implements regular consensus.
    </div>

* Describe an execution that is possible in regular consensus but not uniform consensus.

    __Answer:__
    <div class="hoverover">
    Process P1 decides `1` and then crashes. Later Process P2 decides `0`.
    </div>

---

## Flooding and hierarchical consensus

Assume three processes *p1*, *p2* and *p3* run consensus. *p1* proposes *z*, *p2* proposes *x* and *p3* proposes *y*.
If no processes crash,

* Which value will be chosen by flooding consensus (Alg. 5.1/5.3)?

    __Answer:__
    <div class="hoverover">
    Flooding consensus chooses the minimal value, `min(x,y,z) = x`. Thus `x` will be chosen.
    </div>

* Which value will be chosen by hierarchical consensus (Alg. 5.2/5.4)?

    __Answer:__
    <div class="hoverover">
    Hierarchical consensus chooses the value from the process with highest rank. If process `p1` has highest rank, `z` will be chosen.
    </div>

---

## Consensus in the fail-noisy model

* Explain why any algorithm, that implements regular consensus in the fail-noisy model must implement uniform consensus.

    __Answer:__
    <div class="hoverover">
    In the fail-noisy model, we only have an unreliable failure detector. Thus the processes cannot distinguish the two cases (1) process `p1` crashes and (2) process `p1` does not crash but is suspected by all other processes. In the second case, regular consensus requires `p1` to decide on the same value as the other processes. Thus the same holds for the first case.
    </div>

---

## Randomized consensus

* Why do some algorithms use randomization to solve consensus?

    __Answer:__
    <div class="hoverover">
    It is impossible to solve consensus in the fail-silent model. But using randomization, we can solve consensus without relying on a failure detector (timing assumptions).
    </div>

* Does Randomized binary consensus (Alg. 5.12/5.13) 
impement the agreement property from uniform consensus, or only regular agreement?

    __Answer:__
    <div class="hoverover">
    It actually implements uniform consensus. To see this, either study the algorithm in great detail or do a similar argument as above, showing that to implement regular consensus with randomization, we actually need to implement uniform consensus.
    </div>

---

## Byzantine consensus

* Compare the weak and strong validity properties of byzantine consensus. Does strong validity implement weak validity?
Can byzantine consensus with weak validity be used to implement strong validity?

    __Answer:__
    <div class="hoverover">
    Strong validity does not implement weak validity and vice versa. But strong validity can be implemented given weak validity. See Solution 5.11 in the book.
    </div>

* What triggers a new epoch in Byzantine Leader-Driven Consensus (Alg. 5.19)?

    __Answer:__
    <div class="hoverover">
    All processes maintain a timer. If consensus does not decide within the timeout, the processes complain about the current leader. If `f+1` process complain, a new leader will be elected and a new epoch starts.
    </div>

* Compare the Byzantine Read/Write Epoch Consensus (Alg. 5.17, 5.18) to authenticated double-echo broadcast (Alg. 3.18).

    __Answer:__
    <div class="hoverover">
    The `WRITE` and `ACCEPT` messages in Alg.5.18 are similar to the `ECHO` and `READY` messages in Alg. 3.18. The amplification step, where an `READY` message is sent upon receiving f+1 `ECHO` messages is missing from Alg. 5.18, but it could be introduces as an optimization.
    </div>

A leader for Byzantine Leader-Driven Consensus receives the following messages in the conditional collect primitive:
```html
m1 = <STATE> ts = 2, v = "X", ws = { (ts = 2, v= "X")}  </STATE>
m2 = <STATE> ts = 1, v = "Y", ws = { (ts = 1, v= "Y")}  </STATE>
m3 = <STATE> ts = 0, v = "Y", ws = { }  </STATE>
```
Assume `N=4`.
* Will the leader wait for another message?

    __Answer:__
    <div class="hoverover">
    Let S be the three `<STATE>`-messages from above. `quorumhighest(2,"X", S)` is true, but `iscertified(2, "X", S)` is not true. Thus `binds(2, "X", S)` is false. `unbound(S)` is also false. Thus the leader needs to wait for more messages.
    </div>

Assume the leader receives an additional message:
```html
m4 = <STATE> ts = 0, v = "X", ws = { (ts = 3, v= "Y")}  </STATE>
```
* What value will be written this round?

    __Answer:__
    <div class="hoverover">
    Let `S'={m1, m2, m4}` then `quorumhighest(2,"X", S')` holds and `iscertified(2, "X", S')` holds as well. 
    Thus `binds(2, "X", S')` holds and the value "X" will be written.
    </div>

---

## Extra execises:

* Compare the three algorithms 5.1, 5.2, 5.3 and 5.4. What are the performance benefits drawbacks?
* What optimizations are possible to Read/Write Epoch Consensus (Alg. 5.6) if it is used inside the Leader driven consensus (Alg 5.7).
* Draw sequence charts for binary probabilistic consensus, one for an execution where the coin is tossed, one for an execution, where the coin has no effect.



# Chapter 6
## Total order broadcast (TOB)

1. Causal Order broadcast does not enforce Total order. Give an example for this (sequence diagram).

    __Answer:__
    <div class="hoverover">
    Assume two messages `m1` and ` m2`are sent concurrently by different processes. Using Causal Order broadcast, they can be deliveded in different order, at different processes, e.g. `p1` delivers first `m1` then `m2`, `p2` delivers first `m2` then `m1`.  This violates Total order.
    </div>

2. TOB does not implement FIFO or Causal Order. Give an example for this (sequence diagram).

    __Answer:__
    <div class="hoverover">
    Assume `p1` sends first `m1` then `m2`, before delivering `m1`. FIFO and Causal order require that every process delivers `m1` before `m2`but Total order does not guarantee this.
    </div>

3. In what model can Algorithm 6.1 be used?

    __Answer:__
    <div class="hoverover">
    In all models with crash failures, where we can solve consensus, i.e. fail-stop, fail-noisy and randomized.
    </div>

### Byzantine total order broadcast (BTOB)

4. Which of the properties from Module 6.1 (regular TOB) cannot be achieved with byzantine faulty nodes?

    __Answer:__
    <div class="hoverover">
    No Dublication can only be ensured for correct processes. Also, no creation can only be ensured for correct senders.
    </div>

5. What can go wrong if we replace reliable broadcast in Algorithm 6.1 with their Byzantine variant 
and use weak-byzantine consensus (Module 5.10, p.245) instead of normal consensus?

    __Answer:__
    <div class="hoverover">
    We cannot force a faulty process to propose any specific value. Thus if a faulty process in Alg. 6.1 always proposes the empty set to consensus, this value may be learned an no message will ever be delivered.
    </div>

6. How does Algorithm 6.2 avoid the problems you found in e. above?

    __Answer:__
    <div class="hoverover">
    Alg 6.2 uses Byzantine consensus with the strong validity property, and ensures that the correct processes all propose the same value, when the leader is correct.
    </div>

## Terminating reliable broadcast (TRB)

7. We replace the perfect failure detector in Algorithm 6.3 with an eventually perfect failure detector,
and the `<CRASH>` even with the `<SUSPECT>` event. 
    * Which properties from Module 6.4 still hold? 
    * Can you reformulate the properties that no longer hold?
    
    __Answer:__
    <div class="hoverover">
    With an unreliable failure detector a process may propose the triangle in Alg. 6.3, even if the sender did not crash. Validity may be violated, e.g. a correct sender may broadcast `m`, but if the sender is suspected, the processes may deliver the triangle instead.
    </div>
    
    __Answer:__
    <div class="hoverover">
    __New Validity:__ *If a correct process `p` broadcasts a message `m` then it eventually delivers `m`, or the triangle. If `p` is never suspected by any other process, then `p` eventually delivers `m`.
    </div>

## Fast Consensus
 
 8. Give an example of an execution of Algorithm 6.4, where some process decides on the fast path and 
 another process decides on the slow path.

    __Answer:__
    <div class="hoverover">
    Assume four processes `p1, p2, p3, p4`. Assume `p1`, `p2`, and `p3` propose `0` and `p4`proposes `1`. 
    If a process receives the `<PROPOSAL>` messages from `p1`, `p2`, and `p3` it will decide on `0` on the fast path. 
    If a process receives the `<PROPOSAL>` messages from `p4`, among the first 3 proposals, it will propose `0` to consensus, 
    and eventually decide on `0` on the slow path.
    </div>

## Group membership

9. Consider Algorithm 6.8-6.9. Here the algorithm requests from the application to stop sending messages. 
    * What whould happen if instead, the algorithm would simply stop to forward messages? 
      Which property of Module 6.9 or 6.10 would be violated?
      
    __Answer:__
    <div class="hoverover">
    View Inclusion would be violated. Messages sent in the old view would be delivered first in the new view.
    </div>
    
10. If we implement Algorithm 6.10-6.11 in the *fail-noisy* model with an eventually perfect failure detector (<>P) and use 
    the `<SUSPECT>` event instead of `<CRASH>`, what properties from Module 6.10 would still hold?
    * How can we handle the failure bound *f* and the `<RESTORE>` events from <>P?

    __Answer:__
    <div class="hoverover">
    Using an unreliable failure detector, a process may be removed even if it has not crashed. Thus violating Accuracy of Module 6.8. Also messages sent by the removed process may never be delivered. This may violate reliable delivery.
    </div>
    
## Good execises from the book
I recommend at least reading all exercises from the book. 
The following ones are especially relevant:

* Exercise 6.8
* Exercise 6.9
* Exercise 6.11
* Exercise 6.13

