# Chapter 1 Introduction    (p. 1) 
Motivation

> A distributed system is one in which the failure of a computer you did not even know existed can render your own computer unusable
> <br><br> Leslie Lamport


## 1.1 Motivation 	 (p. 1) 
* Achive Cooperation
* partial failures
* Needs to be Robust to partial failures
* Event more difficult with malicolus behavior
* Simplest: Client-Server computing
    * Well realy centralized
    * Request/response architecture
* General case: Multiparty interaction, 

## 1.2 Distributed Programming Abstractions	 (p. 3)
> Just like the act of smiling, the act of abstracting is restricted to very few natural species. 

* Abstractions helps seperation the fundamental form accessory
* Mainly used to abstractions in the book, processes and links.
    * process could be machine, process, thread, or represent trust domain or principial
    * Link abstract the physical and logical network.
* To be able to communicate, then have to agree on who they are and how to communicate (TCP, UDP, IPC).
* Maybe only take action if everyone agrees.
* If no concenus, step should not happend.
* Sometimes have to agree on content, and order
* Information shearing is a fertile ground for distributed programming abstractions.
* Could be producer (publisher) or consumer (subscriber) of data. Messages &rarr; notification.
    * Types: channel-based, subject based- content-based or type based.
* Some types of data, does not nead all data, f.eks. audio stream (Best effort broadcast).
* Some need all information f.eks. stock exchange (relaiable broadcast)
* Types:
    * Process control
    * Cooperative work
    * Distributed databases.
    * Distributed Storage.
* Some have distribution as an artifact.
    * For instance, fault tolerance, load balancing or fast sharing.
    * can be visualized through state machine replication.
* The easiest way to replicate if all processes receive all the same messages in the same order (Total Order Broadcast). For for instance fault-tolerance in systems, where some nodes could fall down.

## 1.3 The End-to-EndArgument	 (p. 7) 
* Depending on the algorithm the abstraction could vary fro mnegligible to the almost prohibetibe.
* It could be tempting to merge code togheter instead of modules, but that have many disatvantages. Prone to errors.

> Pre mature optimizationis the rootof all evil
>
> DonaldKnuth

## 1.4 Software Components	 (p. 8) 
* Algorithms described as a set of event handlers
* Abstraction usualy described through an api.
* Describing APIs using asyncronous event based composition model.
* A process hosts a set of modules.
* In local process comunication FIFO is enforsed, but for cross process, it is not, my require extra mechanisms.
* Handles only one event at a time.
* "Upon event" &rarr; used for external 
* "upon" &rarr; used for some state satisfaction/condition. 
* Can be merged in a "such that (condition)".
* Two types of events:
    * Requests: invoke a service or signal a condition.
    * Indication: deliver information to another component.
* Algorithms and modules in Code folder.

## 1.5 Classes of Algorithms	 (p. 16) 
From the book:

1. fail-stop algorithms, designed under the assumption that processes can fail by crashingbut the crashes can be reliablydetectedby all the other processes; 
2. fail-silent algorithms,where process crashes can neverbe reliablydetected; 
3. fail-noisy algorithms, where processes can fail by crashing and the crashes can be detected,but notalways in an accurate manner(accuracyis onlyeventual); 
4. fail-recovery algorithms, where processes can crash and later recover and still participatein the algorithm; 
5. fail-arbitrary algorithms, where processes can deviate arbitrarily from the protocol speciﬁcationand act in malicious,adversarialways; and 
6. randomized algorithms, where in addition to the classes presented so far, processes may make probabilisticchoicesby using a source of randomness.
## 1.6 Chapter Notes 	 (p. 17)

# Chapter 2
Assumption about a distributed network.

## 2.1 Distributed Computation 	 (p. 20) 
* Devided into processes and messages
* Every process knows about every other process and is part of &Pi;
* Sometimes used with the id of the process in &Pi; which are abstract.
* Asume that every message is unique
* An automata decides how the process executes it decission steps.
* Excectution is representeed by a sequence of steps.
* A step consists of receiving, executing or sending a message.
* If nothing is sent or executed, nil value or message.
* Local moduels do computation and not messages. cross process = messages.
> The
difficulty is not in talking, or not lying, but in telling the truth.
* Algorithms should contain both.

### Saftey and Liveness
* Saftey property can be violated at time t and never be satisfied again. (Not do anything wrong). 
* If something have happend once, it cant be undone. for instance you cant unsend a message.
* Safest thing is to do nothing.
* Liveness (Eventualy something good happens).
* If a message is sent, it should eventualy be received at the correct place.
* If a message is sent and not received on time t, it has to be proven that no matter how long the time t is, it is never received.
* failure types (worst to "best")
    1. Crash
    2. Omission
    3. Crash with Recovery
    4. Eavesdropping
    5. Arbitrary.

## 2.2 AbstractingProcesses 	 (p. 24) 

* Crash, algorithms stops executing, and never recovers.
* The relationship between allowed faulty processes f and number of processes N is called resilience
* More expensive to tolerate more general failures,
* Byzantine failures is if there for instance is a malicolus attempt.
* Using of cryptography is a way of getting around Eavesdropping.


# Chapter 3
Communication abstraction

# Chapter 4
Shared memory abstraction

# Chapter 5
Consensus abstraction

# Chapter 6
Variants of consensus

# Real titels
1 Introduction	 (p. 1) 
1.1 Motivation 	 (p. 1) 
1.2 Distributed ProgrammingAbstractions	 (p. 3) 
1.3 The End-to-EndArgument	 (p. 7) 
1.4 Software Components	 (p. 8) 
1.5 Classes of Algorithms	 (p. 16) 
1.6 Chapter Notes 	 (p. 17)
2 Basic Abstractions	 (p. 19) 
2.1 Distributed Computation 	 (p. 20) 
2.2 AbstractingProcesses 	 (p. 24) 
2.3 CryptographicAbstractions 	 (p. 30) 
2.4 AbstractingCommunication 	 (p. 32) 
2.5 TimingAssumptions	 (p. 44) 
2.6 AbstractingTime	 (p. 48) 
2.7 Distributed-SystemModels 	 (p. 63) 
2.8 Exercises 	 (p. 67) 
2.9 Solutions 	 (p. 68) 
2.10 Chapter Notes 	 (p. 71)
3 ReliableBroadcast	 (p. 73) 
3.1 Motivation 	 (p. 73) 
3.2 Best-EffortBroadcast 	 (p. 75) 
3.3 RegularReliable Broadcast 	 (p. 77) 
3.4 UniformReliable Broadcast	 (p. 81) 
3.5 StubbornBroadcast	 (p. 85) 
3.6 LoggedBest-Effort Broadcast 	 (p. 87) 
3.7 LoggedUniformReliable Broadcast	 (p. 90) 
3.8 ProbabilisticBroadcast 	 (p. 92) 
3.9 FIFO andCausal Broadcast 	 (p. 100) 
3.10 ByzantineConsistent Broadcast 	 (p. 110) 
3.11 ByzantineReliable Broadcast 	 (p. 116) 
3.12 ByzantineBroadcast Channels	 (p. 120) 
3.13 Exercises 	 (p. 124) 
3.14 Solutions 	 (p. 126) 
3.15 Chapter Notes 	 (p. 134)
4 SharedMemory	 (p. 137) 
4.1 Introduction 	 (p. 138) 
4.2 (1,N)Regular Register 	 (p. 142) 
4.3 (1,N)Atomic Register 	 (p. 149) 
4.4 (N,N) Atomic Register	 (p. 159) 
4.5 (1,N)LoggedRegularRegister 	 (p. 170) 
4.6 (1,N)Byzantine Safe Register 	 (p. 175) 
4.7 (1,N)Byzantine RegularRegister 	 (p. 179) 
4.8 (1,N)Byzantine Atomic Register 	 (p. 188) 
4.9 Exercises 	 (p. 194) 
4.10 Solutions 	 (p. 195) 
4.11 Chapter Notes 	 (p. 200)
5 Consensus	 (p. 203) 
5.1 RegularConsensus 	 (p. 204) 
5.2 UniformConsensus	 (p. 211) 
5.3 UniformConsensus in the Fail-Noisy Model	 (p. 216) 
5.4 LoggedConsensus 	 (p. 228) 
5.5 RandomizedConsensus 	 (p. 235) 
5.6 ByzantineConsensus 	 (p. 244) 
5.7 ByzantineRandomizedConsensus 	 (p. 261) 
5.8 Exercises 	 (p. 266) 
5.9 Solutions 	 (p. 268) 
5.10 Chapter Notes 	 (p. 277)
6 Consensus Variants	 (p. 281) 
6.1 Total-OrderBroadcast	 (p. 281) 
6.2 ByzantineTotal-OrderBroadcast 	 (p. 287) 
6.3 TerminatingReliable Broadcast	 (p. 292) 
6.4 Fast Consensus 	 (p. 296) 
6.5 Fast ByzantineConsensus 	 (p. 300) 
6.6 NonblockingAtomic Commit 	 (p. 303) 
6.7 GroupMembership 	 (p. 307) 
6.8 View-SynchronousCommunication 	 (p. 311) 
6.9 Exercises 	 (p. 323) 
6.10 Solutions 	 (p. 324) 
6.11 Chapter Notes 	 (p. 337)


# All titels
1 Introduction	 
1.1 Motivation 	 (p. 1) 
1.2 Distributed ProgrammingAbstractions	 (p. 3) 
1.2.1 InherentDistribution 	 (p. 4) 
1.2.2 Distribution as an Artifact 	 (p. 6) 
1.3 The End-to-EndArgument	 (p. 7) 
1.4 Software Components	 (p. 8) 
1.4.1 CompositionModel 	 (p. 8) 
1.4.2 ProgrammingInterface 	 (p. 11) 
1.4.3 Modules	 (p. 13) 
1.5 Classes of Algorithms	 (p. 16) 
1.6 Chapter Notes 	 (p. 17)
2 Basic Abstractions	 (p. 19) 
2.1 Distributed Computation 	 (p. 20) 
2.1.1 Processes andMessages 	 (p. 20) 
2.1.2 Automataand Steps 	 (p. 20) 
2.1.3 Safety and Liveness 	 (p. 22) 
2.2 AbstractingProcesses 	 (p. 24) 
2.2.1 Process Failures 	 (p. 24) 
2.2.2 Crashes 	 (p. 24) 
2.2.3 Omissions 	 (p. 26) 
2.2.4 Crashes with Recoveries 	 (p. 26) 
2.2.5 EavesdroppingFaults	 (p. 28) 
2.2.6 ArbitraryFaults 	 (p. 29) 
2.3 CryptographicAbstractions 	 (p. 30) 
2.3.1 Hash Functions 	 (p. 30) 
2.3.2 Message-AuthenticationCodes (MACs) 	 (p. 30) 
2.3.3 Digital Signatures	 (p. 31) 
2.4 AbstractingCommunication 	 (p. 32) 
2.4.1 Link Failures	 (p. 33) 
2.4.2 Fair-Loss Links	 (p. 34) 
2.4.3 StubbornLinks 	 (p. 35) 
2.4.4 Perfect Links	 (p. 37) 
2.4.5 LoggedPerfectLinks	 (p. 38)
2.4.6 AuthenticatedPerfect Links 	 (p. 40) 
2.4.7 On the LinkAbstractions	 (p. 43) 
2.5 TimingAssumptions	 (p. 44) 
2.5.1 AsynchronousSystem 	 (p. 44) 
2.5.2 SynchronousSystem 	 (p. 45) 
2.5.3 Partial Synchrony 	 (p. 47) 
2.6 AbstractingTime	 (p. 48) 
2.6.1 Failure Detection 	 (p. 48) 
2.6.2 Perfect Failure Detection 	 (p. 49) 
2.6.3 LeaderElection	 (p. 51) 
2.6.4 EventuallyPerfect Failure Detection 	 (p. 53) 
2.6.5 EventualLeaderElection	 (p. 56) 
2.6.6 Byzantine LeaderElection 	 (p. 60) 
2.7 Distributed-SystemModels 	 (p. 63) 
2.7.1 CombiningAbstractions 	 (p. 63) 
2.7.2 Setup 	 (p. 64) 
2.7.3 Quorums 	 (p. 65) 
2.7.4 MeasuringPerformance	 (p. 65) 
2.8 Exercises 	 (p. 67) 
2.9 Solutions 	 (p. 68) 
2.10 Chapter Notes 	 (p. 71)
3 ReliableBroadcast	 (p. 73) 
3.1 Motivation 	 (p. 73) 
3.1.1 Client–ServerComputing 	 (p. 73) 
3.1.2 MultiparticipantSystems	 (p. 74) 
3.2 Best-EffortBroadcast 	 (p. 75) 
3.2.1 Speciﬁcation 	 (p. 75) 
3.2.2 Fail-Silent Algorithm:Basic Broadcast 	 (p. 76) 
3.3 RegularReliable Broadcast 	 (p. 77) 
3.3.1 Speciﬁcation 	 (p. 77) 
3.3.2 Fail-Stop Algorithm:LazyReliable Broadcast 	 (p. 78) 
3.3.3 Fail-Silent Algorithm:EagerReliable Broadcast 	 (p. 79) 
3.4 UniformReliable Broadcast	 (p. 81) 
3.4.1 Speciﬁcation 	 (p. 81) 
3.4.2 Fail-Stop Algorithm: All-Ack UniformReliable Broadcast	 (p. 82) 
3.4.3 Fail-Silent Algorithm: Majority-AckUniformReliable Broadcast 	 (p. 84) 
3.5 StubbornBroadcast	 (p. 85) 
3.5.1 Speciﬁcation 	 (p. 85) 
3.5.2 Fail-RecoveryAlgorithm:Basic StubbornBroadcast	 (p. 86) 
3.6 LoggedBest-Effort Broadcast 	 (p. 87) 
3.6.1 Overview 	 (p. 87) 
3.6.2 Speciﬁcation 	 (p. 88) 
3.6.3 Fail-RecoveryAlgorithm:LoggedBasic Broadcast 	 (p. 89)
3.7 LoggedUniformReliable Broadcast	 (p. 90) 
3.7.1 Speciﬁcation 	 (p. 90) 
3.7.2 Fail-RecoveryAlgorithm: LoggedMajority-AckUniformReliable Broadcast 	 (p. 90) 
3.8 ProbabilisticBroadcast 	 (p. 92) 
3.8.1 The Scalability of Reliable Broadcast 	 (p. 92) 
3.8.2 EpidemicDissemination 	 (p. 93) 
3.8.3 Speciﬁcation 	 (p. 94) 
3.8.4 RandomizedAlgorithm:Eager ProbabilisticBroadcast	 (p. 94) 
3.8.5 RandomizedAlgorithm:Lazy Probabilistic Broadcast 	 (p. 97) 
3.9 FIFO andCausal Broadcast 	 (p. 100) 
3.9.1 Overview 	 (p. 101) 
3.9.2 FIFO-OrderSpeciﬁcation 	 (p. 101) 
3.9.3 Fail-Silent Algorithm:Broadcast with SequenceNumber 	 (p. 101) 
3.9.4 Causal-OrderSpeciﬁcation 	 (p. 103) 
3.9.5 Fail-Silent Algorithm:No-Waiting Causal Broadcast 	 (p. 104) 
3.9.6 Fail-Stop Algorithm:Garbage-Collectionof Causal Past 	 (p. 106) 
3.9.7 Fail-Silent Algorithm:Waiting Causal Broadcast	 (p. 108) 
3.10 ByzantineConsistent Broadcast 	 (p. 110) 
3.10.1 Motivation	 (p. 110) 
3.10.2 Speciﬁcation 	 (p. 111) 
3.10.3 Fail-ArbitraryAlgorithm: AuthenticatedEchoBroadcast 	 (p. 112) 
3.10.4 Fail-ArbitraryAlgorithm:Signed EchoBroadcast 	 (p. 114) 
3.11 ByzantineReliable Broadcast 	 (p. 116) 
3.11.1 Speciﬁcation 	 (p. 117) 
3.11.2 Fail-ArbitraryAlgorithm: AuthenticatedDouble-EchoBroadcast 	 (p. 117) 
3.12 ByzantineBroadcast Channels	 (p. 120) 
3.12.1 Speciﬁcations 	 (p. 120) 
3.12.2 Fail-ArbitraryAlgorithm:Byzantine Consistent Channel 	 (p. 122) 
3.12.3 Fail-ArbitraryAlgorithm:Byzantine Reliable Channel 	 (p. 123) 
3.13 Exercises 	 (p. 124) 
3.14 Solutions 	 (p. 126) 
3.15 Chapter Notes 	 (p. 134)
4 SharedMemory	 (p. 137) 
4.1 Introduction 	 (p. 138) 
4.1.1 SharedStorage in a Distributed System	 (p. 138) 
4.1.2 Register Overview 	 (p. 138) 
4.1.3 Completeness andPrecedence 	 (p. 141) 
4.2 (1,N)Regular Register 	 (p. 142) 
4.2.1 Speciﬁcation 	 (p. 142) 
4.2.2 Fail-Stop Algorithm: Read-OneWrite-All RegularRegister 	 (p. 144)
4.2.3 Fail-Silent Algorithm: MajorityVoting Regular Register	 (p. 146) 
4.3 (1,N)Atomic Register 	 (p. 149) 
4.3.1 Speciﬁcation 	 (p. 149) 
4.3.2 Transformation: From (1,N)Regular to (1,N) AtomicRegisters	 (p. 151) 
4.3.3 Fail-Stop Algorithm: Read-ImposeWrite-All(1,N) AtomicRegister 	 (p. 156) 
4.3.4 Fail-Silent Algorithm: Read-ImposeWrite-Majority(1,N)Atomic Register	 (p. 157) 
4.4 (N,N) Atomic Register	 (p. 159) 
4.4.1 Multiple Writers 	 (p. 159) 
4.4.2 Speciﬁcation 	 (p. 160) 
4.4.3 Transformation: From (1,N)Atomic to (N,N) AtomicRegisters 	 (p. 161) 
4.4.4 Fail-Stop Algorithm: Read-ImposeWrite-Consult-All(N,N) Atomic Reg	 (p. 165) 
4.4.5 Fail-Silent Algorithm: Read-ImposeWrite-Consult-Majority(N,N) Atomic Reg. 	 (p. 167) 
4.5 (1,N)LoggedRegularRegister 	 (p. 170) 
4.5.1 Precedencein the Fail-RecoveryModel 	 (p. 170) 
4.5.2 Speciﬁcation 	 (p. 170) 
4.5.3 Fail-RecoveryAlgorithm:LoggedMajority Voting 	 (p. 172) 
4.6 (1,N)Byzantine Safe Register 	 (p. 175) 
4.6.1 Speciﬁcation 	 (p. 176) 
4.6.2 Fail-ArbitraryAlgorithm:Byzantine MaskingQuorum	 (p. 177) 
4.7 (1,N)Byzantine RegularRegister 	 (p. 179) 
4.7.1 Speciﬁcation 	 (p. 179) 
4.7.2 Fail-ArbitraryAlgorithm: Authenticated-DataByzantineQuorum	 (p. 180) 
4.7.3 Fail-ArbitraryAlgorithm: Double-WriteByzantineQuorum	 (p. 182) 
4.8 (1,N)Byzantine Atomic Register 	 (p. 188) 
4.8.1 Speciﬁcation 	 (p. 189) 
4.8.2 Fail-ArbitraryAlgorithm: Byzantine Quorumwith Listeners 	 (p. 189) 
4.9 Exercises 	 (p. 194) 
4.10 Solutions 	 (p. 195) 
4.11 Chapter Notes 	 (p. 200)
5 Consensus	 (p. 203) 
5.1 RegularConsensus 	 (p. 204) 
5.1.1 Speciﬁcation 	 (p. 204) 
5.1.2 Fail-Stop Algorithm:FloodingConsensus 	 (p. 205) 
5.1.3 Fail-Stop Algorithm:HierarchicalConsensus	 (p. 208)
5.2 UniformConsensus	 (p. 211) 
5.2.1 Speciﬁcation 	 (p. 211) 
5.2.2 Fail-Stop Algorithm:FloodingUniformConsensus	 (p. 212) 
5.2.3 Fail-Stop Algorithm:HierarchicalUniformConsensus 	 (p. 213) 
5.3 UniformConsensus in the Fail-Noisy Model	 (p. 216) 
5.3.1 Overview 	 (p. 216) 
5.3.2 Epoch-Change 	 (p. 217) 
5.3.3 EpochConsensus 	 (p. 220) 
5.3.4 Fail-Noisy Algorithm:Leader-DrivenConsensus	 (p. 225) 
5.4 LoggedConsensus 	 (p. 228) 
5.4.1 Speciﬁcation 	 (p. 228) 
5.4.2 LoggedEpoch-Change 	 (p. 229) 
5.4.3 LoggedEpochConsensus 	 (p. 230) 
5.4.4 Fail-RecoveryAlgorithm: LoggedLeader-DrivenConsensus 	 (p. 234) 
5.5 RandomizedConsensus 	 (p. 235) 
5.5.1 Speciﬁcation 	 (p. 236) 
5.5.2 CommonCoin	 (p. 237) 
5.5.3 RandomizedFail-Silent Algorithm: RandomizedBinaryConsensus 	 (p. 238) 
5.5.4 RandomizedFail-Silent Algorithm: RandomizedConsensus with LargeDomain	 (p. 242) 
5.6 ByzantineConsensus 	 (p. 244) 
5.6.1 Speciﬁcations 	 (p. 244) 
5.6.2 Byzantine Epoch-Change 	 (p. 246) 
5.6.3 Byzantine EpochConsensus 	 (p. 248) 
5.6.4 Fail-Noisy-ArbitraryAlgorithm: Byzantine Leader-DrivenConsensus 	 (p. 259) 
5.7 ByzantineRandomizedConsensus 	 (p. 261) 
5.7.1 Speciﬁcation 	 (p. 261) 
5.7.2 RandomizedFail-ArbitraryAlgorithm: Byzantine RandomizedBinary Consensus 	 (p. 261) 
5.8 Exercises 	 (p. 266) 
5.9 Solutions 	 (p. 268) 
5.10 Chapter Notes 	 (p. 277)
6 Consensus Variants	 (p. 281) 
6.1 Total-OrderBroadcast	 (p. 281) 
6.1.1 Overview 	 (p. 281) 
6.1.2 Speciﬁcations 	 (p. 283) 
6.1.3 Fail-Silent Algorithm: Consensus-Based Total-OrderBroadcast	 (p. 284) 
6.2 ByzantineTotal-OrderBroadcast 	 (p. 287) 
6.2.1 Overview 	 (p. 287) 
6.2.2 Speciﬁcation 	 (p. 288)
6.2.3 Fail-Noisy-ArbitraryAlgorithm: Rotating SenderByzantine Broadcast 	 (p. 288) 
6.3 TerminatingReliable Broadcast	 (p. 292) 
6.3.1 Overview 	 (p. 292) 
6.3.2 Speciﬁcation 	 (p. 293) 
6.3.3 Fail-Stop Algorithm:Consensus-Based UniformTerminatingReliable Broadcast 	 (p. 293) 
6.4 Fast Consensus 	 (p. 296) 
6.4.1 Overview 	 (p. 296) 
6.4.2 Speciﬁcation 	 (p. 297) 
6.4.3 Fail-Silent Algorithm: From UniformConsensusto UniformFast Consensus 	 (p. 297) 
6.5 Fast ByzantineConsensus 	 (p. 300) 
6.5.1 Overview 	 (p. 300) 
6.5.2 Speciﬁcation 	 (p. 300) 
6.5.3 Fail-ArbitraryAlgorithm: From ByzantineConsensus to Fast Byzantine Consensus 	 (p. 300) 
6.6 NonblockingAtomic Commit 	 (p. 303) 
6.6.1 Overview 	 (p. 303) 
6.6.2 Speciﬁcation 	 (p. 304) 
6.6.3 Fail-Stop Algorithm: Consensus-Based NonblockingAtomic Commit 	 (p. 304) 
6.7 GroupMembership 	 (p. 307) 
6.7.1 Overview 	 (p. 307) 
6.7.2 Speciﬁcation 	 (p. 308) 
6.7.3 Fail-Stop Algorithm:Consensus-Based GroupMembership 	 (p. 309) 
6.8 View-SynchronousCommunication 	 (p. 311) 
6.8.1 Overview 	 (p. 311) 
6.8.2 Speciﬁcation 	 (p. 312) 
6.8.3 Fail-Stop Algorithm: TRB-Based View-SynchronousCommunication 	 (p. 314) 
6.8.4 Fail-Stop Algorithm:Consensus-Based UniformView-SynchronousCommunication 	 (p. 319) 
6.9 Exercises 	 (p. 323) 
6.10 Solutions 	 (p. 324) 
6.11 Chapter Notes 	 (p. 337)