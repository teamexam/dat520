# Paper assignments:

## slicer-osdi16-adya - Paria Heidari
Summary:

---
## byzcoin-sec16_paper_kokoris-kogias.pdf - Jungwon Seo
Summary:

---
## tapir-zhang-sosp15 - Nicolai Stensland
Summary:

---
## correctables-osdi16-guerraoui - Mari Håland
Summary:

---
## epaxos-moraru-sosp2013 - Magnus Book
Summary:

---
## fastCast (Fast Atomic Multicast) - Fredrik Wigsnes
Summary:

---
## janus-osdi16-mu.pdf - Didrik Fleischer
Summary:

---
## ripple-icdcs2017 - Elisabeth Bratli
Summary:

---
## mojim-ASPLOS2015Mojim - Ferdinand Rødne Tvedt
Summary:

---
## SpeedingUpConsensus - Hafiz Rayaan Shahid
Summary:

---
## snow-osdi16-lu - Kjetil Huy Tran
Summary:

---
## callas-xie-sosp2015 - Luca Tomasetti
Summary:

---
## scalable-smr-2014DSN - Fabian Legland Boe
Summary:

---
## ec-cache-osdi16-rashmi - Daniel Barati
Summary:

---
## solida-opodis2017 - Nicolas Fløysvik
Summary:

---
## nsdi17-vCorfu - Redjol Resulaj
Summary:

---
## hq-replication - Priyanka Chennam Lakshmikumar
Summary:

---
## nsdi17-lepton-storage - Vladyslav Maksyk
Summary:

---
## hybrids_on_steroids_17_eurosys - Anandhakumar Palanisamy
Summary:

---
## hardeningCassandra - Simen Tranøy
Summary:

---
## nonUniformReplication - Rameesha Asghar
Summary:

---
## Dynamic Scalable State Machine Replication - Chibuzor Nwemambu
Summary:

---
## paxos-transparent-crane-sosp15 - Bilal Amjad
Summary:

---
## Low-Overhead Byzantine Fault-Tolerant Storage"/loft-hendricks2007sosp - Magnus Gustavsen
Summary:

---
## consensus-box-nsdi16-istvan - Siavash Rasouli
Summary:

---
## Algorand: Scaling Byzantine Agreements for Cryptocurrencies - Noor Ur Rehman
Summary:

---
## occult-causalDatastore - Zubair Nawaz
Summary:

---
## rifl-lee-sosp2015 - Jerzy Zbigniew Jedruszek
Summary:

---
## xft-osdi16-liu - Stian Trondsen
Summary:

---
## geo-paxos - Kristoffer Løyning Bøe
Summary:

---