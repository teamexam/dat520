# 1.1 Synchronous Job Handler 	14
# 1.2 Asynchronous Job Handler 	14
# 1.3 Job-Transformation by Buffering 	16
# 2.1 Retransmit Forever 	36
# 2.2 Eliminate Duplicates 	38
# 2.3 Log Delivered 	41
# 2.4 Authenticate and Filter 	42
# 2.5 Exclude on Timeout 	51
# 2.6 Monarchical Leader Election 	53
# 2.7 Increasing Timeout 	55
# 2.8 Monarchical Eventual Leader Detection 	57
# 2.9 Elect Lower Epoch 	58
# 2.10 Rotating Byzantine Leader Detection62
# 2.11 Sequence Number 	69
# 3.1 Basic Broadcast 	76
# 3.2 Lazy Reliable Broadcast 	78
# 3.3 Eager Reliable Broadcast 	80
# 3.4 All-Ack Uniform Reliable Broadcast 	83
# 3.5 Majority-Ack Uniform Reliable Broadcast 	85
# 3.6 Basic Stubborn Broadcast 	87
# 3.7 Logged Basic Broadcast 	89
# 3.8 LoggedMajority-Ack Uniform Reliable Broadcast 	91
# 3.9 Eager Probabilistic Broadcast 	95
# 3.10 Lazy Probabilistic Broadcast (part 1, data dissemination) 	98
# 3.11 Lazy Probabilistic Broadcast (part 2, recovery) 	99
# 3.12 Broadcast with Sequence Number 	102
# 3.13 No-Waiting Causal Broadcast 	105
# 3.14 Garbage-Collection of Causal Past (extends Algorithm 3.13) 	107
# 3.15 Waiting Causal Broadcast	108
# 3.16 Authenticated Echo Broadcast 	113
# 3.17 Signed Echo Broadcast 	115
# 3.18 Authenticated Double-Echo Broadcast 	118
# 3.19 Byzantine Consistent Channel 	122
# 3.20 Byzantine Reliable Channel 	123
# 3.21 Simple Optimization of Lazy Reliable Broadcast 	126
# 3.22 Ideal Uniform Reliable Broadcast 	128
# 3.23 Logged Eager Reliable Broadcast 	130
# 3.24 No-Waiting Causal Broadcast using FIFO Broadcast 	132
# 4.1 Read-OneWrite-All 	144
# 4.2 Majority Voting Regular Register 	147
# 4.3 From (1,N) Regular to (1, 1) Atomic Registers 	152
# 4.4 From (1, 1) Atomic to (1,N) Atomic Registers 	154
# 4.5 Read-ImposeWrite-All 	157
# 4.6 Read-ImposeWrite-Majority (part 1, read) 	158
# 4.7 Read-ImposeWrite-Majority (part 2, write and write-back) 	159
# 4.8 From (1,N) Atomic to (N,N) Atomic Registers 	163
# 4.9 Read-ImposeWrite-Consult-All 	166
# 4.10 Read-ImposeWrite-Consult-Majority (part 1, read and consult) 	168
# 4.11 Read-ImposeWrite-Consult-Majority (part 2, write and write-back) 	169
# 4.12 LoggedMajority Voting (part 1, write) 	174
# 4.13 LoggedMajority Voting (part 2, read) 	175
# 4.14 Byzantine Masking Quorum 	178
# 4.15 Authenticated-Data Byzantine Quorum 	181
# 4.16 Double-Write Byzantine Quorum (part 1, write) 	184
# 4.17 Double-Write Byzantine Quorum (part 2, read) 	185
# 4.18 Byzantine Quorum with Listeners (part 1, write) 	190
# 4.19 Byzantine Quorum with Listeners (part 2, read) 	191
# 4.20 Modification of Majority Voting 	196
# 5.1 Flooding Consensus 	206
# 5.2 Hierarchical Consensus 	209
# 5.3 Flooding Uniform Consensus 	213
# 5.4 Hierarchical Uniform Consensus 	214
# 5.5 Leader-Based Epoch-Change 	219
# 5.6 Read/Write Epoch Consensus 	223
# 5.7 Leader-Driven Consensus 	225
# 5.8 Logged Leader-Based Epoch-Change 	231
# 5.9 Logged Read/Write Epoch Consensus 	233
# 5.10 Logged Leader-Driven Consensus (part 1) 	234
# 5.11 Logged Leader-Driven Consensus (part 2) 	235
# 5.12 Randomized Binary Consensus (phase 1) 	239
# 5.13 Randomized Binary Consensus (phase 2) 	240
# 5.14 Randomized Consensus with Large Domain 	243
# 5.15 Byzantine Leader-Based Epoch-Change	248
# 5.16 Signed Conditional Collect 	251
# 5.17 Byzantine Read/Write Epoch Consensus (part 1, read phase) 	252
# 5.18 Byzantine Read/Write Epoch Consensus (part 2, write phase) 	253
# 5.19 Byzantine Leader-Driven Consensus 	259
# 5.20 Byzantine Randomized Binary Consensus (phase 1) 	263
# 5.21 Byzantine Randomized Binary Consensus (phase 2) 	264
# 5.22 Rotating Coordinator (part 1) 	270
# 5.23 Rotating Coordinator (part 2) 	271
# 5.24 From Anchored Validity to Strong Validity 	274
# 5.25 Echo Conditional Collect 	275
# 6.1 Consensus-Based Total-Order Broadcast 	285
# 6.2 Rotating Sender Byzantine Broadcast 	290
# 6.3 Consensus-Based Uniform Terminating Reliable Broadcast 	295
# 6.4 From Uniform Consensus to Uniform Fast Consensus 	298
# 6.5 From Byzantine Consensus to Fast Byzantine Consensus 	302
# 6.6 Consensus-Based Nonblocking Atomic Commit 	306
# 6.7 Consensus-Based Group Membership 	309
# 6.8 TRB-Based View-Synchronous Communication (part 1) 	315
# 6.9 TRB-Based View-Synchronous Communication (part 2) 	316
# 6.10 Consensus-Based Uniform View-Synchronous Comm	(part 1) 	320
# 6.11 Consensus-Based Uniform View-Synchronous Comm	(part 2) 	321
# 6.12 Logged Uniform Total-Order Broadcast 	327
# 6.13 Replicated State Machine using Total-Order Broadcast 	331
# 6.14 Direct Consensus-Based View-Synchronous Comm	(part 1) 	335
# 6.15 Direct Consensus-Based View-Synchronous Comm	(part 2) 	336