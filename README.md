# DAT520
The beginning of the pratice


## Links:
* [Trello Link](https://trello.com/b/mRyu6OFw/dat520)

## How to use this repo:
Here are the links to use this repo, and a short description of where to find what.
|Description | Link |
|------------|------|
|List of all the algorithms and descriptions | [algorithms.md](algorithms.md)|
|Presentation summary| [SummaryOfPresentatinSummaries.md](SummaryOfPresentatinSummaries.md) |
|The book | [Reliable-and-Secure-Distributed-Programming](Book/Reliable-and-Secure-Distributed-Programming.pdf)|
|Exersices|[exercises.md](Excercises/exercises.md)|
|Old exams|[./Old exams/](Old\ exams)|
